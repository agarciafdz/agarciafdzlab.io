#+TITLE: What are your 12 problems
#+DATE: 2021-01-11
#+ROAM_ALIAS:
#+ROAM_TAGS:
#+setupfile: ../org-templates/post.org

My 12 problems are:

* How to create a successful software company?
  I've been trying to solve this problem for the past 20 years here are the current answers:

** Company ownership: Cooperative
   In a tech company the capital is what is in the brains of the developers.
   So how can we have that capital most of the time?
   First of all, eveyone should be an owner of the company.
   Second one there should be interesting problems,
   Third you can improve continously
   Fourth you can select the works that you can work in.

   I think the way to do that is with cooperatives.

** Company management: Theory of Constraints
   TOC provides the tools and thinking tools to keep the company /focused/ on the most importants tasks most of the time.
   So everyone at the cooperative, should learn and dominati toc, with it's tools.

** Company accounting: Throughput accounting.
   Related to the previous point the right way to take accouinting in a software company, is Throughput accounit.
   Because the importance of Throughput and Operating Expenses is the correct one.
   The one thing, you shouldn't do is *Cost Accouinting*
   Cost Accounting is poison for a software company, becuse in Cost Accounting the variable costs are the most important one but in software is the operating expenses.

** Company process management: Kanban
   Kanban is, in my opiinon, the application of TOC thinking and process to the very specific problem of wandering bottlenecks in a "factory" of intangibles.

** Technology: Funcitonal programming with strong types or OOP with strong Design by Contract.
   Strongly typed systems have less bus and are more mainteinable (meaning that you can later improve them without breaking the parts that work)
   Than dynamically typed languages.

*** TODO Insert links to the papers with this.

** Clients: The wourld should be your clients and your production should be local.

** 4 day work week.

   If your a being /productive/ ie delivering results towards a goal, then there shouldn't be a need to work more than 4 days a week.
   And having well rested developers help to create a productive company.

*** TODO Insert link to multiple studies on the 4 day work week.

* How to create successful cloud orchestrations

** ROOMS

** Queue based development

* Multiple representations of the same graph

** Like in HyperPlan, FlyingLogick and Arya's journey.




 
