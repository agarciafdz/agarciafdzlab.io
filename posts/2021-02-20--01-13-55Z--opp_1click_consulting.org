#+TITLE: 1coffee consulting
#+SUBTITLE: solutions now!
#+DATE: 2021-02-19
#+ROAM_ALIAS:
#+ROAM_TAGS:
#+setupfile: ../org-templates/post.org

1coffee.consulting is what happens when you gather 9 technology and innovation experts
around a coffee table and asks them a hard problem.

You get a bunch: high quality highly applicable ideas;
insight (profound understanding) of the problem and solutions,
and access to network of contacts that can implement the solutions.

As Entrepreneurs we frequntly have situations that require an outside expert.
But who would you call? Normally you start with a trusted set of advisors.
So you call your first advisor and she has some idea, but she has a friend who can better help you.
so you ask the friend of your advisor and sure him has another idea.

And so on and so on. But what if you could ask to all the advisors at the same time.
And have them crosspolinate your ideas. Wouldn't that be more effective?
A better use of everyones time and a more joyful experience?

Well that is what we do at 1coffee.consulting. we gather our master mind group
every week willing to taka new and interesting problem possed by one of our frinds / customers.
To provide them with solutions that they haven't gotten before.





#+INCLUDE: ../disquss.inc
